name := """playroom"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies += "org.mockito" % "mockito-core" % "2.4.3"
libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaJpa,
  "org.hibernate" % "hibernate-core" % "5.2.5.Final",
  "dom4j" % "dom4j" % "1.6.1",
  javaWs
)
libraryDependencies += "org.hamcrest" % "hamcrest-all" % "1.3"