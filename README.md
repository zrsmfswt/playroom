Welcome to playroom!
=========================

Project structure:

* **app** - application source code
    * **controllers**
    * **domain** - domain models and core business logic
    * **mappers** - responsible for mapping between domain objects and view models
    * **models** - view models
    * **services** - service layer of the application. Abstracts business processes and hides domain objects from the ui layer
    * **storage** - domain object repositories
    * **validators** - validators for view models
    * **views**
* **conf** - play / jpa configuration files
* **test** - junit tests
    * **builders** - helper classes for building up domain / view model objects used in tests
    * **fakes** - fake implementations used as stubs / mocks
    * **integration** - integration tests focusing on service layer of the application
    * **ui** - system tests using selenium driver
    * **unit** - unit tests - isolation achieved either using fake implementations or Mockito