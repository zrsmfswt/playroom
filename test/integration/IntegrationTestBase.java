package integration;

import org.junit.After;
import org.junit.Before;
import play.Application;
import play.db.jpa.JPA;
import play.test.Helpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class IntegrationTestBase {

    private Application app;

    EntityManager em;
    EntityTransaction tx;

    @Before
    public void beforeEachTest() {
        startFakeApp();
        populateDatabase();
        openTransaction();
    }

    @After
    public void afterEachTest() {
        closeTransaction();
        stopFakeApp();
    }


    private void startFakeApp() {
        app = Helpers.fakeApplication(Helpers.inMemoryDatabase("test"));
        Helpers.start(app);
    }

    private void stopFakeApp() {
        Helpers.stop(app);
    }

    private void openTransaction() {
        em = JPA.em("test");
        JPA.bindForSync(em);
        tx = em.getTransaction();
        tx.begin();
    }

    private void closeTransaction() {
        if (tx != null) {
            if (tx.isActive()) {
                if (tx.getRollbackOnly()) {
                    tx.rollback();
                } else {
                    tx.commit();
                }
            }

        }
        JPA.bindForSync(null);
        if (em != null) {
            em.close();
        }
    }

    private void populateDatabase() {
        openTransaction();
        modifyDatabaseBefore();
        closeTransaction();
    }

    protected void modifyDatabaseBefore() {
        // if you want to modify database before each test
    }
}
