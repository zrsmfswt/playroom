package integration.services;

import domain.AuditRecord;
import domain.User;
import integration.IntegrationTestBase;
import models.AuditRecordModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import services.AuditServiceImpl;
import storage.AuditRepository;
import storage.AuditRepositoryImpl;
import storage.UserRepository;
import storage.UserRepositoryImpl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

// integration tests must inherit from IntegrationTestBase
public class AuditServiceImplTest extends IntegrationTestBase {

    private AuditRepository auditRepository;
    private UserRepository userRepository;

    private AuditServiceImpl service;

    // run before each test
    @Before
    public void Setup() {
        // instantiate all collaborators
        this.auditRepository = new AuditRepositoryImpl();
        this.userRepository = new UserRepositoryImpl();
        // instantiate system under test
        this.service = new AuditServiceImpl(this.auditRepository, this.userRepository);
    }

    @Test
    public void givenNoRecords_whenICallCreateRecord_thenIExpectRecordCreated() {
        // set up a user in the repository
        this.userRepository.createUser("test@test.com", "Test User", "pass");

        // execute method
        this.service.createRecord("test@test.com", "ref", "action", "details");

        // get records from the repository
        List<AuditRecord> auditRecords = this.auditRepository.getAuditRecords(0, 10);
        // assert that we have 1 created record
        Assert.assertEquals(1, auditRecords.size());
        AuditRecord record = auditRecords.get(0);
        // assert state of the record
        Assert.assertEquals("test@test.com", record.getUser().getUsername());
        Assert.assertEquals("ref", record.getReference());
        Assert.assertEquals("action", record.getAction());
        Assert.assertEquals("details", record.getDetails());
    }

    @Test
    public void givenRecordsInDb_whenICallGetRecords_thenIExpectRecordsOrdered() {
        // set up a user in the repository
        User user = this.userRepository.createUser("test@test.com", "Test User", "pass");

        // prepare the state - two audit records with different timestamps
        AuditRecord second = new AuditRecord();
        second.setId(UUID.randomUUID());
        second.setUser(user);
        second.setTimestamp(new GregorianCalendar(2016, Calendar.JANUARY, 1).getTime());
        second.setReference("ref2");
        second.setAction("action");
        this.auditRepository.createRecord(second);

        AuditRecord first = new AuditRecord();
        first.setId(UUID.randomUUID());
        first.setUser(user);
        first.setTimestamp(new GregorianCalendar(2016, Calendar.JANUARY, 2).getTime());
        first.setReference("ref1");
        first.setAction("action");
        this.auditRepository.createRecord(first);

        List<AuditRecordModel> recordModels = this.service.getAuditRecords(0, 10);
        // assert that we have 2 records in the repository
        Assert.assertEquals(2, recordModels.size());
        // assert correct order of records
        AuditRecordModel firstModel = recordModels.get(0);
        AuditRecordModel secondModel = recordModels.get(1);
        Assert.assertEquals("ref1", firstModel.getReference());
        Assert.assertEquals("ref2", secondModel.getReference());
    }
}
