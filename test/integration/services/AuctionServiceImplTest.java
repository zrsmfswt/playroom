package integration.services;

import builders.AuctionBuilder;
import builders.AuctionModelBuilder;
import domain.Auction;
import domain.AuditRecord;
import domain.Bid;
import domain.User;
import integration.IntegrationTestBase;
import models.AuctionModel;
import models.PlaceBidResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import services.AuctionServiceImpl;
import services.AuditServiceImpl;
import storage.*;

import java.util.*;

public class AuctionServiceImplTest extends IntegrationTestBase {

    private UserRepository userRepository;
    private AuctionRepository auctionRepository;
    private AuditRepositoryImpl auditRepository;
    private AuditServiceImpl auditService;

    private AuctionServiceImpl service;

    // run before each test
    @Before
    public void Setup() {
        // instantiate all collaborators
        this.userRepository = new UserRepositoryImpl();
        this.auctionRepository = new AuctionRepositoryImpl();
        this.auditRepository = new AuditRepositoryImpl();
        this.auditService = new AuditServiceImpl(this.auditRepository, this.userRepository);
        // instantiate system under test
        this.service = new AuctionServiceImpl(this.auctionRepository, this.userRepository, this.auditService);
    }

    @Test
    public void givenUserInRepository_andValidAuction_whenICallCreate_thenIExpectAuctionIsCreated() {
        // set up a user in the repository
        this.userRepository.createUser("test@test.com", "Test User", "pass");
        // set up a valid auction model
        AuctionModel givenModel = new AuctionModelBuilder().withValidModel().build();

        // execute method
        AuctionModel result = this.service.createAuction("test@test.com", givenModel);

        // get the created auction from the repository and compare the state with the given model
        Auction created = this.auctionRepository.findById(result.getId());
        Assert.assertEquals(givenModel.getItem().getName(), created.getItem().getName());
        Assert.assertEquals(givenModel.getItem().getDescription(), created.getItem().getDescription());
        Assert.assertEquals(givenModel.getStartDate(), created.getStartDate());
        Assert.assertEquals(givenModel.getEndDate(), created.getEndDate());
        Assert.assertEquals(givenModel.getMinAmount(), created.getMinAmount(), 0.1);
        Assert.assertEquals("test@test.com", created.getSeller().getUsername());

        // assert that a audit record has been created with correct data
        List<AuditRecord> auditRecords = this.auditRepository.getAuditRecords(0, 10);
        Assert.assertEquals(1, auditRecords.size());
        AuditRecord audit = auditRecords.get(0);
        Assert.assertEquals(created.getId().toString(), audit.getReference());
        Assert.assertEquals("test@test.com", audit.getUser().getUsername());
        Assert.assertEquals("Auction created", audit.getAction());
    }

    @Test
    public void givenAuctionWithNoBids_whenICallPlaceBidWithValidAmount_thenIExpectBidAdded() {
        // set up seller and bidder in user repository
        User seller = this.userRepository.createUser("test@test.com", "Test User", "pass");
        User bidder = this.userRepository.createUser("bidder@test.com", "Test Bidder", "pass");

        // set up an existing auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setSeller(seller))
                .build();
        this.auctionRepository.createOrUpdate(auction);

        // execute method
        this.service.placeBid(auction.getId(), "bidder@test.com", 10);

        // assert that the state of the auction has changed as expected
        Assert.assertEquals(1, auction.getBids().size());
        Assert.assertNotNull(auction.getCurrentBid());
        Assert.assertEquals(1, auction.getCurrentBid().getAmount(), 0.1);
        Assert.assertEquals("bidder@test.com", auction.getCurrentBid().getBidder().getUsername());

        // assert that a audit record has been created with correct data
        AuditRecord record = this.auditRepository.getAuditRecords(0, 10).get(0);
        Assert.assertEquals(auction.getId().toString(), record.getReference());
        Assert.assertEquals("bidder@test.com", record.getUser().getUsername());
        Assert.assertEquals("New bid placed", record.getAction());
        Assert.assertEquals("Max amount placed: 10.00, bid won, previous bid value: none, current bid value: 1.00", record.getDetails());
    }

    @Test
    public void givenAuctionWith1Bid_whenICallPlaceBidWithLesserAmount_thenIExpectBidChangesCurrentValue() {
        // set up seller, initial bidder and new bidder
        User seller = this.userRepository.createUser("test@test.com", "Test User", "pass");
        User bidder = this.userRepository.createUser("bidder@test.com", "Test Bidder", "pass");
        User newBidder = this.userRepository.createUser("newbidder@test.com", "New Bidder", "pass");

        // set up existing bid
        Bid existing = new Bid();
        existing.setId(UUID.randomUUID());
        existing.setAmount(30);
        existing.setMaxAmount(100);
        existing.setBidder(bidder);
        List<Bid> bids = new ArrayList<Bid>();
        bids.add(existing);

        // set up auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setSeller(seller))
                .with(a -> a.setCurrentBid(existing))
                .with(a -> a.setBids(bids))
                .build();
        this.auctionRepository.createOrUpdate(auction);

        // execute method
        PlaceBidResponse response = this.service.placeBid(auction.getId(), "newbidder@test.com", 50);

        // assert return value (response) and changed auction state
        Assert.assertEquals(false, response.doesBidWin());
        Assert.assertEquals(2, auction.getBids().size());
        Assert.assertEquals(60, auction.getCurrentBid().getAmount(), 0.1);
        Assert.assertEquals("bidder@test.com", auction.getCurrentBid().getBidder().getUsername());

        // assert that a audit record has been created with correct data
        AuditRecord record = this.auditRepository.getAuditRecords(0, 10).get(0);
        Assert.assertEquals(auction.getId().toString(), record.getReference());
        Assert.assertEquals("newbidder@test.com", record.getUser().getUsername());
        Assert.assertEquals("New bid placed", record.getAction());
        Assert.assertEquals("Max amount placed: 50.00, bid lost, previous bid value: 30.00, current bid value: 60.00", record.getDetails());
    }

    @Test
    public void givenAuctionWith1Bid_whenICallPlaceBidWithHigherAmount_thenIExpectBidWins() {
        // set up seller, initial bidder and new bidder
        User seller = this.userRepository.createUser("test@test.com", "Test User", "pass");
        User bidder = this.userRepository.createUser("bidder@test.com", "Test Bidder", "pass");
        User newBidder = this.userRepository.createUser("newbidder@test.com", "New Bidder", "pass");

        // set up existing bid
        Bid existing = new Bid();
        existing.setId(UUID.randomUUID());
        existing.setAmount(30);
        existing.setMaxAmount(100);
        existing.setBidder(bidder);
        List<Bid> bids = new ArrayList<Bid>();
        bids.add(existing);

        // set up auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setSeller(seller))
                .with(a -> a.setCurrentBid(existing))
                .with(a -> a.setBids(bids))
                .build();
        this.auctionRepository.createOrUpdate(auction);

        // execute method
        PlaceBidResponse response = this.service.placeBid(auction.getId(), "newbidder@test.com", 200);

        // assert return value (response) and changed auction state
        Assert.assertEquals(true, response.doesBidWin());
        Assert.assertEquals(2, auction.getBids().size());
        Assert.assertEquals(110, auction.getCurrentBid().getAmount(), 0.1);
        Assert.assertEquals("newbidder@test.com", auction.getCurrentBid().getBidder().getUsername());

        // assert that a audit record has been created with correct data
        AuditRecord record = this.auditRepository.getAuditRecords(0, 10).get(0);
        Assert.assertEquals(auction.getId().toString(), record.getReference());
        Assert.assertEquals("newbidder@test.com", record.getUser().getUsername());
        Assert.assertEquals("New bid placed", record.getAction());
        Assert.assertEquals("Max amount placed: 200.00, bid won, previous bid value: 30.00, current bid value: 110.00", record.getDetails());
    }
}
