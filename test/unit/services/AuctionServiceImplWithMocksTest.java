package unit.services;

import builders.AuctionBuilder;
import builders.AuctionModelBuilder;
import domain.Auction;
import domain.AuctionState;
import domain.BidCalculator;
import domain.User;
import models.AuctionModel;
import models.PlaceBidResponse;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import services.AuctionServiceImpl;
import services.AuditService;
import storage.AuctionRepository;
import storage.UserRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.reflections.util.ConfigurationBuilder.build;

public class AuctionServiceImplWithMocksTest {

    @Rule
    // JUnit rule which will be asserted after execution of each test
    // we use it to assert that exceptions are thrown in certain scenarios
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void givenValidAuctionAndUserInRepository_whenICallCreate_thenIExpectAuctionIsCreated() {
        // instantiate auction repository mock - we will need the reference to assert interaction
        AuctionRepository auctionRepositoryMock = mock(AuctionRepository.class);

        // mock initial state of the collaborators
        // crate a user and set up findByUsername method for our UserRepository mock
        UserRepository userRepositoryMock = mock(UserRepository.class);
        User user = new User();
        user.setUsername("user@test.com");
        user.setName("Test User");
        when(userRepositoryMock.findByUsername(anyString()))
                .thenReturn(user);

        // prepare parameters for the method under test
        // create a valid auction model
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .build();

        // instantiate system under test and call the method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(auctionRepositoryMock, userRepositoryMock, mock(AuditService.class));
        AuctionModel result = auctionService.createAuction(user.getUsername(), auctionModel);

        // argument captor is used to capture the parameter passed to the mocked method
        ArgumentCaptor<Auction> auctionCaptor = ArgumentCaptor.forClass(Auction.class);
        // assert that createOrUpdated has been called on our AuctionRepository mock object and use the captor to capture the passed parameter
        verify(auctionRepositoryMock).createOrUpdate(auctionCaptor.capture());
        // assert that correct parameter was passed to our mocked method
        Assert.assertEquals("auction should be created with state OPEN", AuctionState.OPEN, auctionCaptor.getValue().getState());
        // assert that the return value is not null
        Assert.assertNotNull(result);
    }

    @Test
    public void givenInvalidUsername_whenICallCreate_thenIExpectExceptionThrown() {
        // set up a valid auction model, we simulate only invalid username parameter
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .build();

        // expected exception is prepared before execution
        // expect exception type
        this.exception.expect(IllegalArgumentException.class);

        // expect exception message
        this.exception.expectMessage("username must be defined");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), mock(UserRepository.class), mock(AuditService.class));
        auctionService.createAuction(null, auctionModel);
    }

    @Test
    public void givenInvalidAuctionModel_whenICallCreate_thenIExpectExceptionThrown() {
        // set up an invalid model - we can set that the item is not defined for the auction
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(a ->  a.setItem(null))
                .build();

        // crate a user and set up findByUsername method for our UserRepository mock
        UserRepository userRepositoryMock = mock(UserRepository.class);
        User user = new User();
        user.setUsername("user@test.com");
        user.setName("Test User");
        when(userRepositoryMock.findByUsername(anyString()))
                .thenReturn(user);

        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("auction model is not valid");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), userRepositoryMock, mock(AuditService.class));
        auctionService.createAuction("user@test.com", auctionModel);
    }

    @Test
    public void givenUserNotExists_whenICallCreate_thenIExpectExceptionThrown() {
        // set up a valid model
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .build();

        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("user with the given username doesn't exist");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), mock(UserRepository.class), mock(AuditService.class));
        auctionService.createAuction("user@test.com", auctionModel);
    }

    @Test
    public void givenParameterIdIsNull_whenICallPlaceBid_thenIExpectExceptionThrown() {
        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("auction id must be defined");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), mock(UserRepository.class), mock(AuditService.class));
        auctionService.placeBid(null, "test@test.com", 10);
    }

    @Test
    public void givenParameterUsernameIsNull_whenICallPlaceBid_thenIExpectExceptionThrown() {
        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("username must be defined");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), mock(UserRepository.class), mock(AuditService.class));
        auctionService.placeBid(UUID.randomUUID(), null, 10);
    }

    @Test
    public void givenAuctionDoesntExistInRepository_whenICallPlaceBid_thenIExpectExceptionThrown() {
        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("auction with the specified id doesn't exists");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(mock(AuctionRepository.class), mock(UserRepository.class), mock(AuditService.class));
        auctionService.placeBid(UUID.randomUUID(), "test@test.com", 10);
    }

    @Test
    public void givenAuctionNotInStateOpen_whenICallPlaceBid_thenIExpectExceptionThrown() {
        // set up AuctionRepository mock and auction in state CLOSED
        AuctionRepository auctionRepositoryMock = mock(AuctionRepository.class);
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setState(AuctionState.CLOSED))
                .build();
        // for any UUID return prepared auction
        when(auctionRepositoryMock.findById(any(UUID.class))).thenReturn(auction);

        // set up exception expectations
        this.exception.expect(IllegalStateException.class);
        this.exception.expectMessage("auction is currently not active");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(auctionRepositoryMock, mock(UserRepository.class), mock(AuditService.class));
        auctionService.placeBid(UUID.randomUUID(), "test@test.com", 10);
    }

    @Test
    public void givenUserDoesntExistInRepository_whenICallPlaceBid_thenIExpectExceptionThrown() {
        // set up AuctionRepository mock and auction in state CLOSED
        AuctionRepository auctionRepositoryMock = mock(AuctionRepository.class);
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .build();
        // for any UUID return prepared auction
        when(auctionRepositoryMock.findById(any(UUID.class))).thenReturn(auction);

        // set up exception expectations
        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("user with the given username doesn't exist");

        // instantiate system under test and execute method
        AuctionServiceImpl auctionService = new AuctionServiceImpl(auctionRepositoryMock, mock(UserRepository.class), mock(AuditService.class));
        auctionService.placeBid(UUID.randomUUID(), "test@test.com", 10);
    }

    @Test
    public void givenExistingValidAuction_whenICallClose_thenIExpectAuctionClosed() {
        // initialize mock object
        AuctionRepository auctionRepositoryMock = mock(AuctionRepository.class);

        // mock initial state of collaborators
        // set up an auction that is in state OPEN and has expired
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, -1);
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setEndDate(cal.getTime()))
                .with(a -> a.setState(AuctionState.OPEN))
                .build();
        when(auctionRepositoryMock.findById(any(UUID.class))).thenReturn(auction);

        // instantiate system under test and call method
        // we create two mocks inline because we won't use them for asserting afterwards and no additional setup is needed
        AuctionServiceImpl service = new AuctionServiceImpl(auctionRepositoryMock, mock(UserRepository.class), mock(AuditService.class));
        service.closeAuction(UUID.randomUUID());

        // assert that the state of the auction is now closed
        Assert.assertEquals("auction is not closed", AuctionState.CLOSED, auction.getState());
        // assert that createOrUpdate method is called on our AuctionRepository mock object
        verify(auctionRepositoryMock).createOrUpdate(any(Auction.class));
    }
}
