package unit.services;

import builders.AuctionModelBuilder;
import domain.Auction;
import domain.User;
import fakes.AuctionRepositoryFakeImpl;
import fakes.AuditServiceFakeImpl;
import fakes.UserRepositoryFakeImpl;
import models.AuctionItemModel;
import models.AuctionModel;
import org.junit.*;
import org.junit.rules.ExpectedException;
import services.AuctionServiceImpl;


public class AuctionServiceImplWithFakesTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void givenValidAuctionAndUserInRepository_whenICallCreate_thenIExpectAuctionIsCreated() {
        AuctionRepositoryFakeImpl auctionRepositoryFake = new AuctionRepositoryFakeImpl();
        UserRepositoryFakeImpl userRepositoryFake = new UserRepositoryFakeImpl();
        AuditServiceFakeImpl auditServiceFake = new AuditServiceFakeImpl();
        User user = new User();
        user.setUsername("user@test.com");
        user.setName("Test User");
        userRepositoryFake.addUser(user);
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .build();

        AuctionServiceImpl auctionService = new AuctionServiceImpl(auctionRepositoryFake, userRepositoryFake, auditServiceFake);
        auctionService.createAuction(user.getUsername(), auctionModel);

        Auction created = auctionRepositoryFake.findAll().stream().findFirst().orElse(null);
        Assert.assertNotNull("auction not created", created);
    }

    @Test
    public void givenNullUsername_whenICallCreate_ThenIExpectExceptionThrown() {
        AuctionRepositoryFakeImpl auctionRepositoryFake = new AuctionRepositoryFakeImpl();
        UserRepositoryFakeImpl userRepositoryFake = new UserRepositoryFakeImpl();
        AuditServiceFakeImpl auditServiceFake = new AuditServiceFakeImpl();

        AuctionServiceImpl auctionService = new AuctionServiceImpl(auctionRepositoryFake, userRepositoryFake, auditServiceFake);

        this.exception.expect(IllegalArgumentException.class);
        this.exception.expectMessage("username must be defined");

        auctionService.createAuction(null, new AuctionModel());
    }
}
