package unit.validators;

import builders.AuctionModelBuilder;
import models.AuctionItemModel;
import models.AuctionModel;
import org.junit.*;
import validators.AuctionModelValidator;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AuctionModelValidatorTest {

    @Test
    public void givenNullModelArgument_whenICallIsValid_thenIExpectFalse() {
        Assert.assertFalse(AuctionModelValidator.isValid(null));
    }

    @Test
    public void givenValidModel_whenICallIsValid_thenIExpectTrue() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .build();

        Assert.assertTrue(AuctionModelValidator.isValid(auctionModel));
    }

    @Test
    public void givenStartDateGreaterThanEndDate_whenICallIsValid_thenIExpectFalse() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(am -> am.setStartDate(new GregorianCalendar(2015, Calendar.FEBRUARY, 1).getTime()))
                .with(am -> am.setEndDate(new GregorianCalendar(2015, Calendar.JANUARY, 1).getTime()))
                .build();

        Assert.assertFalse(AuctionModelValidator.isValid(auctionModel));
    }

    @Test
    public void givenAuctionItemIsNull_whenICallIsValid_thenIExpectFalse() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(am -> am.setItem(null))
                .build();

        Assert.assertFalse(AuctionModelValidator.isValid(auctionModel));
    }

    @Test
    public void givenAuctionItemNameIsNull_whenICallIsValid_thenIExpectFalse() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(am -> am.getItem().setName(null))
                .build();

        Assert.assertFalse(AuctionModelValidator.isValid(auctionModel));
    }

    @Test
    public void givenAuctionItemDescriptionIsNull_whenICallIsValid_thenIExpectFalse() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(am -> am.getItem().setDescription(null))
                .build();

        Assert.assertFalse(AuctionModelValidator.isValid(auctionModel));
    }

    @Test
    public void givenMinAmountIsLessThan1_whenICallIsValid_thenIExpectFalse() {
        AuctionModel auctionModel = new AuctionModelBuilder()
                .withValidModel()
                .with(am -> am.setMinAmount(0))
                .build();

        Assert.assertFalse(AuctionModelValidator.isValid(auctionModel));
    }

}
