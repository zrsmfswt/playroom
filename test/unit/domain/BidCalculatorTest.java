package unit.domain;

import builders.AuctionBuilder;
import domain.Auction;
import domain.Bid;
import domain.BidCalculator;
import models.PlaceBidResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class BidCalculatorTest {

    @Test
    public void givenBidWithLesserAmountThanMin_whenITryPlaceBid_thenIExpectNegativeResponse() {
        // set up an auction with min amount
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(10))
                .build();

        // set up a bid with lesser max amount than auction min amount
        Bid newBid = new Bid();
        newBid.setMaxAmount(5);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid didn't win
        Assert.assertFalse(response.doesBidWin());
        // assert correct message
        Assert.assertEquals("Bid amount must be greater than specified min amount", response.getMessage());
        // assert that auction still doesn't have a current bid
        Assert.assertNull(auction.getCurrentBid());
    }

    @Test
    public void givenFirstBidWithValidMaxAmount_whenITryPlaceBid_thenIExpectBidWins() {
        // set up an auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(5))
                .build();

        // set up a bid
        Bid newBid = new Bid();
        newBid.setMaxAmount(10);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid won
        Assert.assertTrue(response.doesBidWin());
        // assert that new bid is the current bid
        Assert.assertEquals(newBid, auction.getCurrentBid());
        // assert that amount of the current bid is min auction amount
        Assert.assertEquals(auction.getMinAmount(), newBid.getAmount(), 0.1);
    }

    @Test
    public void givenSecondBidWithMaxAmountLesserThanCurrentMaxAmount_andIncrementInRange_whenITryPlaceBid_thenIExpectBidLost_andCurrentBidIncreasedWithIncrement() {
        // set up an existing bid
        Bid existingBid = new Bid();
        existingBid.setMaxAmount(40);
        existingBid.setAmount(5);

        ArrayList<Bid> auctionBids = new ArrayList<Bid>();
        auctionBids.add(existingBid);

        // set up an auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(5))
                .with(a -> a.setCurrentBid(existingBid))
                .with(a -> a.setBids(auctionBids))
                .build();

        // set up a new bid
        Bid newBid = new Bid();
        newBid.setMaxAmount(20);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid didn't win
        Assert.assertFalse(response.doesBidWin());
        // assert correct message
        Assert.assertEquals("There is another bid with greater max amount", response.getMessage());
        // assert existing bid is still current bid
        Assert.assertEquals(existingBid, auction.getCurrentBid());
        // assert current bid increased
        Assert.assertEquals(30, auction.getCurrentBid().getAmount(), 0.1);
    }

    @Test
    public void givenSecondBidWithMaxAmountLesserThanCurrentMaxAmount_andIncrementOutOfRange_whenITryPlaceBid_thenIExpectBidLost_andCurrentBidIncreasedToMax() {
        // set up an existing bid
        Bid existingBid = new Bid();
        existingBid.setMaxAmount(35);
        existingBid.setAmount(5);

        ArrayList<Bid> auctionBids = new ArrayList<Bid>();
        auctionBids.add(existingBid);

        // set up an auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(5))
                .with(a -> a.setCurrentBid(existingBid))
                .with(a -> a.setBids(auctionBids))
                .build();

        // set up a new bid
        Bid newBid = new Bid();
        newBid.setMaxAmount(30);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid didn't win
        Assert.assertFalse(response.doesBidWin());
        // assert existing bid is still current bid
        Assert.assertEquals(existingBid, auction.getCurrentBid());
        // assert current bid increased
        Assert.assertEquals(35, auction.getCurrentBid().getAmount(), 0.1);
    }

    @Test
    public void givenSecondBidWithMaxAmountGreaterThanCurrentMaxAmount_AndIncrementInRange_whenITryPlaceBid_thenIExpectBidWins_andCurrentBidIncreasedWithIncrement() {
        // set up an existing bid
        Bid existingBid = new Bid();
        existingBid.setMaxAmount(20);
        existingBid.setAmount(5);

        ArrayList<Bid> auctionBids = new ArrayList<Bid>();
        auctionBids.add(existingBid);

        // set up an auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(5))
                .with(a -> a.setCurrentBid(existingBid))
                .with(a -> a.setBids(auctionBids))
                .build();

        // set up a new bid
        Bid newBid = new Bid();
        newBid.setMaxAmount(40);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid won
        Assert.assertTrue(response.doesBidWin());
        // assert new bid is now current bid
        Assert.assertEquals(newBid, auction.getCurrentBid());
        // assert current bid increased
        Assert.assertEquals(30, auction.getCurrentBid().getAmount(), 0.1);
    }

    @Test
    public void givenSecondBidWithMaxAmountGreaterThanCurrentMaxAmount_AndIncrementOutOfRange_whenITryPlaceBid_thenIExpectBidWins_andCurrentBidIncreasedToMaxAmount() {
        // set up an existing bid
        Bid existingBid = new Bid();
        existingBid.setMaxAmount(20);
        existingBid.setAmount(5);

        ArrayList<Bid> auctionBids = new ArrayList<Bid>();
        auctionBids.add(existingBid);

        // set up an auction
        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setMinAmount(5))
                .with(a -> a.setCurrentBid(existingBid))
                .with(a -> a.setBids(auctionBids))
                .build();

        // set up a new bid
        Bid newBid = new Bid();
        newBid.setMaxAmount(25);

        // instantiate system under test and call method
        BidCalculator calculator = new BidCalculator(auction);
        PlaceBidResponse response = calculator.TryPlaceBid(newBid);

        // assert that bid won
        Assert.assertTrue(response.doesBidWin());
        // assert new bid is now current bid
        Assert.assertEquals(newBid, auction.getCurrentBid());
        // assert current bid increased
        Assert.assertEquals(25, auction.getCurrentBid().getAmount(), 0.1);
    }
}
