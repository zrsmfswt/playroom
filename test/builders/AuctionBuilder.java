package builders;

import domain.Auction;
import domain.AuctionItem;
import domain.AuctionState;
import domain.User;
import models.AuctionItemModel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

public class AuctionBuilder extends BuilderBase<Auction> {

    public AuctionBuilder() {
        this.object = new Auction();
    }

    public AuctionBuilder withValidEntity() {
        User seller = new User();
        seller.setName("Seller");
        seller.setUsername("seller@test.com");
        AuctionItem item = new AuctionItem();
        item.setName("Item name");
        item.setDescription("Item description");
        this.object.setId(UUID.randomUUID());
        this.object.setItem(item);
        this.object.setSeller(seller);
        this.object.setMinAmount(1);
        Date now = new Date();
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(now);
        calStart.add(Calendar.DATE, -1);
        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(now);
        calEnd.add(Calendar.DATE, 1);
        this.object.setStartDate(calStart.getTime());
        this.object.setEndDate(calEnd.getTime());
        this.object.setState(AuctionState.OPEN);
        return this;
    }
}
