package builders;

import java.util.function.Consumer;

public abstract class BuilderBase<T> {

    protected T object;

    public BuilderBase<T> with(Consumer<T> consumer) {
        consumer.accept(this.object);
        return this;
    }

    public T build() {
        return this.object;
    }
}
