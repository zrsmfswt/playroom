package builders;

import models.AuctionItemModel;
import models.AuctionModel;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.Consumer;

public class AuctionModelBuilder extends BuilderBase<AuctionModel> {

    public AuctionModelBuilder() {
        this.object = new AuctionModel();
    }

    public AuctionModelBuilder withValidModel() {
        AuctionItemModel itemModel = new AuctionItemModel();
        itemModel.setName("Item name");
        itemModel.setDescription("Item description");
        this.object.setMinAmount(1);
        this.object.setItem(itemModel);
        Date now = new Date();
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(now);
        calStart.add(Calendar.DATE, -1);
        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(now);
        calEnd.add(Calendar.DATE, 1);
        this.object.setStartDate(calStart.getTime());
        this.object.setEndDate(calEnd.getTime());
        return this;
    }
}
