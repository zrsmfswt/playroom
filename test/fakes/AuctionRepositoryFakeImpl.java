package fakes;

import domain.Auction;
import storage.AuctionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AuctionRepositoryFakeImpl implements AuctionRepository {

    private List<Auction> auctions;

    public AuctionRepositoryFakeImpl() {
        this.auctions = new ArrayList<Auction>();
    }

    public void addAuction(Auction auction) {
        this.auctions.add(auction);
    }

    @Override
    public List<Auction> findAll() {
        return this.auctions;
    }

    @Override
    public Auction findById(UUID id) {
        return this.auctions
                .stream()
                .filter(a -> a.getId() == id)
                .findFirst()
                .orElse(null);
    }

    @Override
    public void createOrUpdate(Auction auction) {
        this.auctions.add(auction);
    }
}
