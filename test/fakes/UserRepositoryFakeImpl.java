package fakes;

import domain.User;
import storage.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryFakeImpl implements UserRepository {
    private List<User> users;

    public UserRepositoryFakeImpl() {
        this.users = new ArrayList<User>();
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    @Override
    public User findByUsername(String username) {
        return this.users
                .stream()
                .filter(u -> u.getUsername().equalsIgnoreCase(username))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User createUser(String username, String name, String password) {
        User user = new User();
        user.setUsername(username);
        user.setName(name);
        user.setPassword(password);
        this.users.add(user);
        return user;
    }

    @Override
    public List<User> getUsers() {
        return this.users;
    }
}
