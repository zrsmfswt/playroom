package fakes;

import models.AuditRecordModel;
import services.AuditService;

import java.util.ArrayList;
import java.util.List;

public class AuditServiceFakeImpl implements AuditService {

    private List<AuditRecordModel> records;

    public AuditServiceFakeImpl() {
        this.records = new ArrayList<AuditRecordModel>();
    }

    @Override
    public List<AuditRecordModel> getAuditRecords(int skip, int page) {
        return this.records;
    }

    @Override
    public void createRecord(String username, String reference, String action, String details) {
        AuditRecordModel model = new AuditRecordModel();
        model.setUsername(username);
        model.setNameOfUser(username);
        model.setReference(reference);
        model.setAction(action);
        model.setDetails(details);
        this.records.add(model);
    }

    @Override
    public void createRecord(String username, String reference, String action) {
        this.createRecord(username, reference, action, null);
    }
}
