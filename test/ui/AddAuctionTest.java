package ui;

import builders.AuctionBuilder;
import domain.Auction;
import domain.User;
import integration.IntegrationTestBase;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import play.db.jpa.Transactional;
import scala.App;
import storage.AuctionRepository;
import storage.AuctionRepositoryImpl;
import storage.UserRepository;
import storage.UserRepositoryImpl;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.*;

public class AddAuctionTest extends UITestBase {

    @Override
    protected void modifyDatabaseBefore() {
        UserRepository userRepository = new UserRepositoryImpl();
        userRepository.createUser("test@test.com", "Test User", "test");
    }

   @Test
    public void AddAuctionTest(){
       browser.goTo("/");

       WebDriver driver = browser.getDriver();
       WebElement username = driver.findElement(By.id("data-test-username"));
       username.sendKeys("test@test.com");
       WebElement password = driver.findElement(By.id("data-test-password"));
       password.sendKeys("test");
       WebElement loginButton = driver.findElement(By.id("button-test-login"));
       loginButton.click();

       WebDriverWait wait = new WebDriverWait(driver, 10);
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("data-test-link")));
       WebElement href = driver.findElement(By.id("data-test-link"));
       href.click();
       WebElement itemName = driver.findElement(By.id("data-test-item-name"));
       itemName.sendKeys("New Item Name");
       WebElement itemDesc = driver.findElement(By.id("data-test-item-desc"));
       itemDesc.sendKeys("New item description");
       WebElement minAmount = driver.findElement(By.id("data-test-min-amount"));
       minAmount.sendKeys("150");
       WebElement addAuctionButton = driver.findElement(By.id("data-test-add-item"));
       addAuctionButton.click();
       List<WebElement> auctions = driver.findElements(By.className("data-test-auction"));
       assertEquals(1, auctions.size());
    }
}
