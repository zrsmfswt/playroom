package ui;

import domain.Auction;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.BrowserType;
import play.Application;
import play.db.jpa.JPA;
import play.test.Helpers;
import play.test.TestBrowser;
import play.test.TestServer;
import play.test.WithBrowser;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static play.test.Helpers.FIREFOX;
import static play.test.Helpers.HTMLUNIT;

public class UITestBase extends WithBrowser {

    EntityManager em;
    EntityTransaction tx;

    @Override
    protected Application provideApplication() {
        return Helpers.fakeApplication();
    }

    @Override
    protected int providePort() {
        return 4050;
    }

    @Override
    protected TestBrowser provideBrowser(int port) {
        return Helpers.testBrowser(HTMLUNIT, port);
    }

    @Before
    public void beforeEachTest() {
        openTransaction();
        cleanup();
        modifyDatabaseBefore();
        closeTransaction();
    }

    protected void modifyDatabaseBefore() {
        // if you want to modify database before each test
    }

    private void cleanup() {
        em.createQuery("delete from Auction").executeUpdate();
        em.createQuery("delete from Bid").executeUpdate();
        em.createQuery("delete from AuditRecord").executeUpdate();
        em.createQuery("delete from User").executeUpdate();
    }

    private void openTransaction() {
        em = JPA.em("default");
        JPA.bindForSync(em);
        tx = em.getTransaction();
        tx.begin();
    }

    private void closeTransaction() {
        if (tx != null) {
            if (tx.isActive()) {
                if (tx.getRollbackOnly()) {
                    tx.rollback();
                } else {
                    tx.commit();
                }
            }
        }
        JPA.bindForSync(null);
        if (em != null) {
            em.close();
        }
    }
}
