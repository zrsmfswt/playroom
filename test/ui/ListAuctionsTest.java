package ui;

import builders.AuctionBuilder;
import domain.Auction;
import domain.User;
import models.AuctionModel;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import play.Application;
import storage.AuctionRepository;
import storage.AuctionRepositoryImpl;
import storage.UserRepository;
import storage.UserRepositoryImpl;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;
import static play.test.Helpers.HTMLUNIT;

public class ListAuctionsTest extends UITestBase {

    @Override
    protected void modifyDatabaseBefore() {
        UserRepository userRepository = new UserRepositoryImpl();
        User user = userRepository.createUser("test@test.com", "Test User", "test");

        Auction auction = new AuctionBuilder()
                .withValidEntity()
                .with(a -> a.setSeller(user))
                .build();

        AuctionRepository auctionRepository = new AuctionRepositoryImpl();
        auctionRepository.createOrUpdate(auction);
    }

    @Test
    public void ListAuctionsTest(){
        browser.goTo("/");

        WebDriver driver = browser.getDriver();
        WebElement username = driver.findElement(By.id("data-test-username"));
        username.sendKeys("test@test.com");
        WebElement password = driver.findElement(By.id("data-test-password"));
        password.sendKeys("test");
        WebElement loginButton = driver.findElement(By.id("button-test-login"));
        loginButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("data-test-auction")));

        List<WebElement> auctions = driver.findElements(By.className("data-test-auction"));
        assertEquals(1, auctions.size());
    }
}
