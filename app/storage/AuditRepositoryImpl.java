package storage;

import domain.AuditRecord;
import play.db.jpa.JPA;

import java.util.List;

public class AuditRepositoryImpl implements AuditRepository {
    @Override
    public List<AuditRecord> getAuditRecords(int startFrom, int numberOfResults) {
        return JPA.em().createQuery("select ar from AuditRecord ar order by ar.timestamp desc", AuditRecord.class)
                .setFirstResult(startFrom)
                .setMaxResults(numberOfResults)
                .getResultList();
    }

    @Override
    public void createRecord(AuditRecord auditRecord) {
        JPA.em().persist(auditRecord);
    }
}
