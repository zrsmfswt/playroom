package storage;

import domain.User;

import java.util.List;

/**
 * User repository.
 */
public interface UserRepository {
    /**
     * Find user by username.
     * @param username  Username of the requested user.
     * @return          Instance of {@link User}.
     */
    User findByUsername(String username);

    /**
     * Create and store new user.
     * @param username  Username.
     * @param name      Name of the user.
     * @param password  Password.
     * @return          Instance of {@link User}.
     */
    User createUser(String username, String name, String password);

    /**
     * Get all users.
     * @return  Collection of {@link User}.
     */
    List<User> getUsers();
}
