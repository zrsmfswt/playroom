package storage;

import domain.AuditRecord;

import java.util.List;

/**
 * Audit record repository.
 */
public interface AuditRepository {
    /**
     * Get all audit records ordered by timestamp descending.
     * @param startFrom         Index of first result returned.
     * @param numberOfResults   Maximum number of results to return.
     * @return                  Collection of {@link AuditRecord}
     */
    List<AuditRecord> getAuditRecords(int startFrom, int numberOfResults);

    /**
     * Stores audit record.
     * @param auditRecord   Instance of {@link AuditRecord}.
     */
    void createRecord(AuditRecord auditRecord);
}
