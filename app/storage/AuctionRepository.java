package storage;

import domain.Auction;

import java.util.List;
import java.util.UUID;

/**
 * Auction repository.
 */
public interface AuctionRepository {
    /**
     * Get all auctions.
     * @return  Collection of {@link Auction}.
     */
    List<Auction> findAll();

    /**
     * Get auction by id.
     * @param id    Auction id.
     * @return      Instance of {@link Auction}.
     */
    Auction findById(UUID id);

    /**
     * Create new or update existing auction.
     * @param auction   Instance of {@link Auction}.
     */
    void createOrUpdate(Auction auction);
}
