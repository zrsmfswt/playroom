package storage;

import domain.Auction;
import play.db.jpa.JPA;

import java.util.List;
import java.util.UUID;

public class AuctionRepositoryImpl implements AuctionRepository {
    @Override
    public List<Auction> findAll() {
        return JPA.em().createQuery("select a from Auction a", Auction.class).getResultList();
    }

    @Override
    public Auction findById(UUID id) {
        return JPA.em().find(Auction.class, id);
    }

    @Override
    public void createOrUpdate(Auction auction) {
        JPA.em().persist(auction);
    }
}
