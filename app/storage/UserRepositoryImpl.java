package storage;

import domain.User;
import play.db.jpa.JPA;

import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public User findByUsername(String username) {
        return JPA.em().find(User.class, username);
    }

    @Override
    public User createUser(String username, String name, String password) {
        User user = this.findByUsername(username);
        if(user == null){
            user = new User();
        }
        user.setUsername(username);
        user.setName(name);
        user.setPassword(password);
        JPA.em().persist(user);
        return user;
    }

    @Override
    public List<User> getUsers() {
        return JPA.em().createQuery("select u from User u", User.class).getResultList();
    }
}
