package domain;

public enum AuctionState {
    PENDING,
    OPEN,
    CLOSED;
}
