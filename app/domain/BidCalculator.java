package domain;

import models.PlaceBidResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the incremental bidding logic.
 */
public class BidCalculator {
    private Auction auction;
    public static final float increment = 10;

    public BidCalculator(Auction auction) {
        this.auction = auction;
    }

    /**
     * Handles newly created bid.
     * @param bid   Newly placed bid.
     * @return      Instance of {@link PlaceBidResponse}.
     */
    public PlaceBidResponse TryPlaceBid(Bid bid) {
        Bid current = auction.getCurrentBid();

        if (current == null) {
            if (bid.getMaxAmount() < auction.getMinAmount()) {
                return new PlaceBidResponse(false, "Bid amount must be greater than specified min amount");
            }
            bid.setAmount(auction.getMinAmount());
            List<Bid> bids = new ArrayList<Bid>();
            bids.add(bid);
            auction.setBids(bids);
            auction.setCurrentBid(bid);
            return new PlaceBidResponse(true);
        }

        if (current.getMaxAmount() > bid.getMaxAmount()) {
            float newAmount = bid.getMaxAmount() + increment > current.getMaxAmount() ? current.getMaxAmount() : bid.getMaxAmount() + increment;
            current.setAmount(newAmount);
            bid.setAmount(bid.getMaxAmount());
            auction.getBids().add(bid);
            return new PlaceBidResponse(false, "There is another bid with greater max amount");
        }

        float newAmount = current.getMaxAmount() + increment > bid.getMaxAmount() ? bid.getMaxAmount() : current.getMaxAmount() + increment;
        bid.setAmount(newAmount);
        auction.setCurrentBid(bid);
        auction.getBids().add(bid);
        return new PlaceBidResponse(true);
    }
}
