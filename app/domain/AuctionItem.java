package domain;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class AuctionItem {
    @Id
    private UUID id;
    @MapsId
    @OneToOne(mappedBy = "item")
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Auction auction;
    private String name;
    private String description;

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
