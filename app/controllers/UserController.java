package controllers;

import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import storage.UserRepository;
import storage.UserRepositoryImpl;
import views.html.*;

public class UserController extends Controller {

    @Transactional(readOnly = true)
    public Result index() {
        UserRepository repo = new UserRepositoryImpl();
        return ok(users.render(repo.getUsers()));
    }

    @Transactional
    public Result create() {
        UserRepository repo = new UserRepositoryImpl();
        repo.createUser("test@test.com", "Test User", "Test");
        repo.createUser("test2@test.com", "Test User", "Test");
        return ok();
    }
}
