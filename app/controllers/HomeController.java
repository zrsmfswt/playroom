package controllers;

import com.google.inject.Inject;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.*;

import services.AuctionService;
import services.UserService;
import views.html.*;

import java.util.Map;
import java.util.UUID;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private UserService userService;
    private AuctionService auctionService;

    @Inject
    public HomeController(UserService userService, AuctionService auctionService){

        this.userService = userService;
        this.auctionService = auctionService;
    }
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render(""));
    }
    @Transactional
    public Result login() {
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String username = dynamicForm.get("username");
        String password = dynamicForm.get("password");
        if(this.userService.login(username, password)){
            session("user", username);
            return ok(auctions.render(this.auctionService.getAll(), ""));
        }
        return ok(index.render("Wrong password"));
    }

    @Transactional
    public Result bid(){
        String user = session("user");
        if(user == null){
            return ok(index.render("Please login please"));
        }
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        UUID id = UUID.fromString(dynamicForm.get("id"));

        float bid = Float.parseFloat(dynamicForm.get("bid"));
        this.auctionService.placeBid(id, user, bid);
        return ok(auctions.render(this.auctionService.getAll(), ""));
    }
}
