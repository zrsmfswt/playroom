package controllers;

import com.google.inject.Inject;
import models.AuctionItemModel;
import models.AuctionModel;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import services.AuctionService;
import services.AuctionServiceImpl;
import services.UserService;
import views.html.*;

import java.util.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class AddAuctionController extends Controller {

    private AuctionService auctionService;

    @Inject
    public AddAuctionController( AuctionService auctionService){
        this.auctionService = auctionService;
    }
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(addauction.render());
    }
    @Transactional
    public Result login() {
       return ok(addauction.render());
    }

    @Transactional
    public Result addAuction() {
        String user = session("user");
        if(user == null){
            return ok(index.render("Please login please"));
        }
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        String name = dynamicForm.get("name");
        String desc = dynamicForm.get("desc");
        String minAmount = dynamicForm.get("min");
        AuctionModel auctionModel = new AuctionModel();

        auctionModel.setStartDate(new Date());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +7);

        auctionModel.setEndDate(cal.getTime());
        auctionModel.setMinAmount(Integer.parseInt(minAmount));

        AuctionItemModel item = new AuctionItemModel();
        item.setDescription(desc);
        item.setName(name);

        auctionModel.setItem(item);

        this.auctionService.createAuction(user, auctionModel);

        return ok(auctions.render(this.auctionService.getAll(), "New auction added"));

    }
}
