package mappers;

import domain.AuditRecord;
import models.AuditRecordModel;

public class AuditRecordMapper {
    public static AuditRecordModel ToModel(AuditRecord entity) {
        AuditRecordModel model = new AuditRecordModel();
        model.setTimestamp(entity.getTimestamp());
        model.setReference(entity.getReference());
        model.setAction(entity.getAction());
        model.setDetails(entity.getDetails());
        model.setUsername(entity.getUser().getUsername());
        model.setNameOfUser(entity.getUser().getName());
        return model;
    }
}
