package mappers;

import domain.Auction;
import domain.AuctionItem;
import domain.User;
import models.AuctionItemModel;
import models.AuctionModel;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class AuctionMapper {
    public static Auction mapToEntity(AuctionModel model, User seller) {
        AuctionItem item = new AuctionItem();
        item.setName(model.getItem().getName());
        item.setDescription(model.getItem().getDescription());

        Auction auction = new Auction();
        auction.setId(model.getId());
        auction.setStartDate(model.getStartDate());
        auction.setEndDate(model.getEndDate());
        auction.setMinAmount(model.getMinAmount());
        auction.setSeller(seller);
        auction.setItem(item);

        return auction;
    }

    public static AuctionModel mapToModel(Auction auction) {
        AuctionItemModel itemModel = new AuctionItemModel();
        itemModel.setName(auction.getItem().getName());
        itemModel.setDescription(auction.getItem().getDescription());

        AuctionModel auctionModel = new AuctionModel();
        auctionModel.setId(auction.getId());
        auctionModel.setStartDate(auction.getStartDate());
        auctionModel.setEndDate(auction.getEndDate());
        auctionModel.setMinAmount(auction.getMinAmount());
        auctionModel.setItem(itemModel);
        if(auction.getCurrentBid() != null){
            auctionModel.setAmount(auction.getCurrentBid().getAmount());
        }else{
            auctionModel.setAmount(0);
        }
        return auctionModel;
    }

    public static List<AuctionModel> mapToModel(List<Auction> auctions){
        List<AuctionModel> retList = new LinkedList<AuctionModel>();
        for (Auction auction: auctions) {
            retList.add(AuctionMapper.mapToModel(auction));
        }
        return retList;
    }
}
