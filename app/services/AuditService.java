package services;

import models.AuditRecordModel;

import java.util.List;

/**
 * Service for performing auditing operations.
 */
public interface AuditService {
    /**
     * Gets audit records ordered by timestamp descending.
     * @param startFrom         Index of first result returned.
     * @param numberOfResults   Maximum number of results to return.
     * @return                  Collection of {@link AuditRecordModel}.
     */
    List<AuditRecordModel> getAuditRecords(int startFrom, int numberOfResults);

    /**
     * Create and store new audit record.
     * @param username  Username of the user that triggered the action.
     * @param reference Performed action reference.
     * @param action    Performed action description.
     * @param details   Additional details associated with the action.
     */
    void createRecord(String username, String reference, String action, String details);

    /**
     * Create and store new audit record.
     * @param username  Username of the user that triggered the action.
     * @param reference Performed action reference.
     * @param action    Performed action description.
     */
    void createRecord(String username, String reference, String action);
}
