package services;

import com.google.inject.Inject;
import domain.User;
import storage.UserRepository;

public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    @Inject
    public UserServiceImpl(UserRepository userRepository){

        this.userRepository = userRepository;
    }

    @Override
    public boolean login(String username, String password) {
        User user = this.userRepository.findByUsername(username);
        if(user == null){
            return false;
        }
        return user.getPassword().equals(password);
    }
}
