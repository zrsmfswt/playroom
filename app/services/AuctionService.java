package services;

import domain.Auction;
import models.AuctionModel;
import models.PlaceBidResponse;

import java.util.List;
import java.util.UUID;

/**
 * Service that provides methods for performing auction related actions.
 */
public interface AuctionService {
    /**
     * Creates and stores a new auction.
     * @param username      Seller username.
     * @param auctionModel  Instance of {@link AuctionModel}.
     * @return              Updated instance of {@link AuctionModel}.
     */
    AuctionModel createAuction(String username, AuctionModel auctionModel);

    /**
     * Tries to place a new bid for an existing auction.
     * @param id        Auction id.
     * @param username  Bidder username.
     * @param maxAmount Max amount offered.
     * @return          Instance of {@link PlaceBidResponse}.
     */
    PlaceBidResponse placeBid(UUID id, String username, float maxAmount);

    /**
     * Closes an auction if the time has expired.
     * @param id    Auction id.
     */
    void closeAuction(UUID id);
    List<AuctionModel> getAll();
}
