package services;

/**
 * Service for handling user-specific actions.
 */
public interface UserService {
    /**
     * Tries to log in a user.
     * @param username  Username.
     * @param password  Password.
     * @return          True if user with provided credentials exists in the system.
     */
    public boolean login(String username, String password);
}
