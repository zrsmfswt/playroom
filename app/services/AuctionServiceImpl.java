package services;

import com.google.inject.Inject;
import domain.*;
import mappers.AuctionMapper;
import models.AuctionModel;
import models.PlaceBidResponse;
import org.apache.commons.lang3.StringUtils;
import storage.AuctionRepository;
import storage.UserRepository;
import validators.AuctionModelValidator;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

public class AuctionServiceImpl implements AuctionService {
    private AuctionRepository auctionRepository;
    private UserRepository userRepository;
    private AuditService auditService;

    @Inject
    public AuctionServiceImpl(AuctionRepository auctionRepository, UserRepository userRepository, AuditService auditService) {
        this.auctionRepository = auctionRepository;
        this.userRepository = userRepository;
        this.auditService = auditService;
    }

    @Override
    public AuctionModel createAuction(String username, AuctionModel auctionModel) {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username must be defined");
        }
        if (!AuctionModelValidator.isValid(auctionModel)) {
            throw new IllegalArgumentException("auction model is not valid");
        }
        User seller = this.userRepository.findByUsername(username);
        if (seller == null) {
            throw new IllegalArgumentException("user with the given username doesn't exist");
        }
        Auction auction = AuctionMapper.mapToEntity(auctionModel, seller);
        auction.setId(UUID.randomUUID());
        Date now = new Date();
        if (now.compareTo(auction.getStartDate()) >= 0) {
            auction.setState(AuctionState.OPEN);
        }
        else {
            auction.setState(AuctionState.PENDING);
        }
        this.auctionRepository.createOrUpdate(auction);
        this.auditService.createRecord(username, auction.getId().toString(), "Auction created");

        return AuctionMapper.mapToModel(auction);
    }

    @Override
    public PlaceBidResponse placeBid(UUID id, String username, float maxAmount) {
        if (id == null) {
            throw new IllegalArgumentException("auction id must be defined");
        }
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username must be defined");
        }
        Auction auction = this.auctionRepository.findById(id);
        if (auction == null) {
            throw new IllegalArgumentException("auction with the specified id doesn't exists");
        }

        if (auction.getSeller().getUsername().compareTo(username) == 0) {
            throw new IllegalArgumentException("users can't place bids on owned auctions");
        }

        if (auction.getState() != AuctionState.OPEN) {
            throw new IllegalStateException("auction is currently not active");
        }
        User bidder = this.userRepository.findByUsername(username);
        if (bidder == null) {
            throw new IllegalArgumentException("user with the given username doesn't exist");
        }

        float previousBidValue = auction.getCurrentBid() != null ? auction.getCurrentBid().getAmount() : -1;

        Bid newBid = new Bid();
        newBid.setId(UUID.randomUUID());
        newBid.setMaxAmount(maxAmount);
        newBid.setBidder(bidder);
        BidCalculator bidCalculator = new BidCalculator(auction);
        PlaceBidResponse response = bidCalculator.TryPlaceBid(newBid);

        this.auctionRepository.createOrUpdate(auction);

        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append(String.format("Max amount placed: %.2f", newBid.getMaxAmount()));
        if (response.doesBidWin()) {
            messageBuilder.append(", bid won");
        }
        else {
            messageBuilder.append(", bid lost");
        }
        messageBuilder.append(", previous bid value: ");
        if (previousBidValue > 0) {
            messageBuilder.append(String.format("%.2f", previousBidValue));
        }
        else {
            messageBuilder.append("none");
        }
        messageBuilder.append(", current bid value: ");
        if (auction.getCurrentBid() != null) {
            messageBuilder.append(String.format("%.2f", auction.getCurrentBid().getAmount()));
        }
        else {
            messageBuilder.append("none");
        }

        this.auditService.createRecord(username, auction.getId().toString(), "New bid placed", messageBuilder.toString());

        return response;
    }

    @Override
    public void closeAuction(UUID id) {
        if (id == null) {
            throw new IllegalArgumentException("auction id must be defined");
        }
        Auction auction = this.auctionRepository.findById(id);
        if (auction == null) {
            throw new IllegalArgumentException("auction with the specified id doesn't exists");
        }
        if (auction.getState() == AuctionState.CLOSED) {
            throw new IllegalStateException("auction is already closed");
        }
        Date now = new Date();
        if (now.compareTo(auction.getEndDate()) < 0) {
            throw new IllegalStateException("auction can't be closed until end date has passed");
        }
        auction.setState(AuctionState.CLOSED);
        this.auctionRepository.createOrUpdate(auction);
    }

    @Override
    public List<AuctionModel> getAll() {
        List<Auction> auctions= this.auctionRepository.findAll();
        return AuctionMapper.mapToModel(auctions);
    }
}