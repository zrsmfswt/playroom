package services;

import com.google.inject.Inject;
import domain.AuditRecord;
import domain.User;
import mappers.AuditRecordMapper;
import models.AuditRecordModel;
import org.apache.commons.lang3.StringUtils;
import storage.AuditRepository;
import storage.UserRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AuditServiceImpl implements AuditService {

    private AuditRepository auditRepository;
    private UserRepository userRepository;
    @Inject
    public AuditServiceImpl(AuditRepository auditRepository, UserRepository userRepository) {

        this.auditRepository = auditRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<AuditRecordModel> getAuditRecords(int startFrom, int numberOfResults) {
        List<AuditRecord> records = this.auditRepository.getAuditRecords(startFrom, numberOfResults);
        return records.stream().map(r -> AuditRecordMapper.ToModel(r)).collect(Collectors.toList());
    }

    @Override
    public void createRecord(String username, String reference, String action, String details) {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username must be defined");
        }

        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException("user with the given username doesn't exist");
        }

        if (StringUtils.isEmpty(reference)) {
            throw new IllegalArgumentException("reference must be defined");
        }

        if (StringUtils.isEmpty(action)) {
            throw new IllegalArgumentException("action must be defined");
        }

        AuditRecord auditRecord = new AuditRecord();
        auditRecord.setId(UUID.randomUUID());
        auditRecord.setTimestamp(new Date());
        auditRecord.setReference(reference);
        auditRecord.setUser(user);
        auditRecord.setAction(action);
        auditRecord.setDetails(details);

        this.auditRepository.createRecord(auditRecord);
    }

    @Override
    public void createRecord(String username, String reference, String action) {
        this.createRecord(username, reference, action, null);
    }
}
