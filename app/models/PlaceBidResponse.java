package models;

public class PlaceBidResponse {
    private String message;
    private boolean bidWins;

    public PlaceBidResponse(boolean bidWins) {
        this(bidWins, null);
    }

    public PlaceBidResponse(boolean bidWins, String message) {
        this.message = message;
        this.bidWins = bidWins;
    }

    public boolean doesBidWin() {
        return bidWins;
    }

    public String getMessage() {
        return message;
    }
}
