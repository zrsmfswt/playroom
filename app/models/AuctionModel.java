package models;

import domain.AuctionItem;

import java.util.Date;
import java.util.UUID;

public class AuctionModel {
    private Date startDate;
    private Date endDate;
    private AuctionItemModel item;
    private UUID id;
    private float minAmount;
    private float amount;

    public float getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(float minAmount) {
        this.minAmount = minAmount;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public AuctionItemModel getItem() {
        return item;
    }

    public void setItem(AuctionItemModel item) {
        this.item = item;
    }

    public float getAmount(){return amount;}

    public void setAmount(float amount) {this.amount = amount;}
}
