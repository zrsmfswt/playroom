package validators;

import models.AuctionItemModel;
import models.AuctionModel;
import org.apache.commons.lang3.StringUtils;

public class AuctionModelValidator {
    public static boolean isValid(AuctionModel auctionModel) {
        if (auctionModel == null) {
            return false;
        }
        if (auctionModel.getStartDate().compareTo(auctionModel.getEndDate()) > 0) {
            return false;
        }

        if (auctionModel.getMinAmount() < 1) {
            return false;
        }

        AuctionItemModel item = auctionModel.getItem();
        if (item == null) {
            return false;
        }
        if (StringUtils.isEmpty(item.getName())) {
            return false;
        }
        if (StringUtils.isEmpty(item.getDescription())) {
            return false;
        }
        return true;
    }
}
